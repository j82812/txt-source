「合……合……合合合合合合合合合合！」

Thistle　Porcupine把背上的針全都射了出去，從圓滾滾的可愛野獸模樣變得十分苗條，口中卻仍發出像出了問題的錄音檔跳針似的連發叫聲。

站在一旁的Cassis　Moose用他的大手輕輕在她背上一拍，Thistle就咳了好一會兒，然後才深深吸進一口氣。

「…………合併～～～～～～～？」

最後發出超高音域的尖叫。

美早與仁子等耳鳴消退後，同時點了點頭。

「YES。」

「沒錯。」

緊接著Thistle就身體一晃。三獸士當中，就屬她對於和黑暗星雲合作的做法表示出最多疑慮，所以也不是不能體會她會大受震撼，但還是非得讓她理解不可。

Thistle儘管虛擬身體往後傾斜了二十度左右，但仍勉強踏住了腳步，雙手往前伸，頻頻搖頭說道：

「不……不不不不，等一下。呃……說起來，讓軍團合併這種事情，系統上允許嗎？」

美早對總算稍微鎮定了些的豪豬點了點頭。

「可以。只是，有領土的軍團要互相合併，領土就必須相鄰。」

「這個情形下，像是軍團名稱、還有軍團長，會怎麼決定？」

第二個問題則由仁子回答：

「這個部分好像是有各種選項可以自由選擇。可以保留其中一個軍團的名稱，也可以取個新名字，可以由其中一個軍團長繼續擔任軍團長，也可以選出新的軍團長。」

「哦……——聽說前陣子世田谷戰區有幾個人加入了黑暗星雲，他們是怎樣的情形？」

「啊，她們不是合併，是先解散原本的軍團，重新加入黑暗星雲。」

「是喔？為什麼要特地弄得這麼麻煩？」

「誰知道呢……我也沒問這麼多。」

就在這個Thistle與仁子不約而同歪了歪頭的時機，先前一直保持沉默的Cassis　Moose發言了：

「大概會共享『處決攻擊』吧……」

「這話怎麼說？」

Cassis把頭歪向另一邊。

「我也是這次去查過才知道的……當兩個軍團合併，無論各種選項怎麼選，原本的兩個軍團長都還會保留一個月的……處決攻擊發動權。」

「是喔……這也就是說，如果合併後的軍團選出一個新的軍團長，那麼包括原本的兩個前任軍團長在內，暫時會有三個人可以處決團員？」

「看樣子是這樣。」

「嗚嗯，這下如果合併後爭執起來，就會弄得處決大放送啦！」

「這種大放送也太討厭了吧。」

仁子聽著Cassis與Thistle的對話，苦笑著打岔：

「這也就是說，Petit　Paquet的前軍團長，是為了不讓處決的權利留在自己身上，才特意先解散了軍團？還真有膽識呢。」

「等等等等等等！現在不是佩服的時候吧，Rain！」

Thistle再度拉高音調嚷嚷起來。

「你該不會要讓我們軍團也這麼做吧？我才不要搞得像是我們被合併吸收一樣！」

「波奇，你冷靜點。」

Cassis把雙手放到Thistle肩上，嬌小的豪豬就再度慢慢冷卻下來。最後她重重呼出一口氣，然後抬頭對這個高大的駝鹿翻白眼。

「……Cassis你也太鎮定了吧……啊，你該不會從一開始就知道合併的事了？」

「這……這個嘛，這些小事就別在意了。」

Cassis一邊清了清嗓子，一邊放開雙手，退開一步。

Thistle　Porcupine的推測是對的。美早與仁子在這場會議之前，就先只把軍團合併的可能性告知了Cassis　Moose。她們這麼做的理由，是希望他能幫忙安撫燃點低的Thistle，但Cassis還自行幫忙查了不少事情。

美早擔任新生日珥副團長奮戰至今的這段不短的日子裡，就曾經屢次得到他冷靜的判斷力，以及Thistle臨機應變的爆發力幫助。他們兩人都是軍團不可或缺的最高幹部，而且也深深愛著這個軍團。

他們對軍團的愛，也許更勝過美早。

美早身為「三獸士」的忠誠心，主要並非投注在軍團，而是投注在仁子個人身上。而美早身為超頻連線者的熱情，則是指向個人戰多於領土戰爭。她之所以會頻繁地出入被譽為對戰聖地的秋葉原對戰競技場，收集情報固然是目的之一，但想磨練一對一的對戰技術，也占了很大一部分。

因此美早對於和黑暗星雲合併這件事並不怎麼抗拒。日珥的名稱消失固然令她遺憾，但只要有仁子在的地方，就是美早的戰場，何況這次的合併方針是仁子深思熟慮之後才決定的。

然而對大多數的日珥團員來說，多半沒有辦法這麼輕易地接受這件事。即使是從第二代團長的時代才開始嶄露頭角的Thistle，都受到這麼大的震撼，從前任團長時代就加入的老資格團員Blaze　Heart與Peach　Parasol等人，多半不會只是嚇一跳而已。因為不管怎麼說，砍下前任團長紅之王Red　Rider的首級，逼得他點數全失的人，正是黑暗星雲的現任軍團長，黑之王BlackLotus——

「波奇，我也知道要和黑暗星雲合併，這件事讓人很難接受。」

仁子朝著默默讓一身淡紫色毛皮呈波浪狀翻動的Thistle　Porcupine，靜靜地開始訴說：

「當然，我也沒打算解散日珥，單方面讓對方吸收。我打算好好交涉，讓這次合併以對等方式進行。只是……就算是這樣，坦白說，我沒把握能讓現在的所有團員都願意追隨我。相信多得是團員會想退出……而且即使波奇和卡西做出這個選擇，我也沒有權利責怪你們……」

「………………」

Thistlena也不想就要回話，但Cassis再度輕輕按住妯。仁子直視他們倆，繼續說道：

「只是……我最近常常在想。我在上一代退場後就進入動蕩戰國時代的練馬和池袋這一帶，為了活下去而拚命戰鬥，為的是什麼……從我被拱上第二代紅之王的立場後，也一直咬緊牙關保護領土，又是為了什麼。我這個人，根本就不適合扮演那種領導一大群人的角色，而且老實說，我對日珥這塊招牌也不是那麼有感情。根本就不必有什麼領土，只要能和幾個知心的朋友一起，創個小小的軍團，在加速世界的角落開開心心地玩下去就好了……」

深紅色的少女型虛擬角色，仰望轟雷空間裡灰蒙蒙的天空。她就像在厚重的雲層後頭找到了飛鳥似的，輕輕伸出右手。

「我想，我一定是不想逃避。不想逃避那些想從瀕臨瓦解的紅之團身上大撈一票點數而跑來攻擊的傢伙……也不想逃避那些大軍團裡看不起我，覺得我終究不是純色，只是個冒牌王的傢伙……還有，更不想逃避願意依靠我的現在這些日珥團員。我一直覺得自己只是在虛張聲勢，遲早有一天我的鍍金會被剝開，讓大家看到我的難堪樣，讓我怕得不得了……可是，我心中卻實實在在有著不想逃避，不想認輸的心情……」

相信讓仁子察覺這一點的並不是美早，而是半年前才認識的那只銀色的烏鴉。這件事讓美早感覺到等量的落寞與欣喜，她豎起耳朵，不想漏聽這位年幼的軍團長所說的一字一句。

「——所以我這次也想咬牙撐下去。坦白說，加速研究社，還有自之團，我都很怕。可是，他們是害死Cherry的仇人……同時也是我的仇人。要是在適個時候逃避，把事情全都交給黑暗星雲那些人去處理，我就再也沒有資格自稱是王了。這次，我一定要憑自己的意志，為了保護自己該保護的事物而戰……波奇、卡西，拜託你們，助我一臂之力。」

仁子斬釘截鐵地說完最後這句話，就朝「三獸士」當中的這兩人用力一鞠躬。

Thistle讓幾乎已經長回原來長度的毛皮甩出一波很大的波浪，深深吸一口氣，然後以平靜的聲調質問軍團長：

「Rain，你說你該保護的東西，是什麼？」

仁子站直身體，以毫不動搖的聲調回答：

「我自己，和日珥的伙伴們……還有加速世界本身。」

接著這次換Cassis甩動巨大的角問起：

「黑之王Black　Lotus，應該是在追求升上10級……也就是把BRAIN　BURST玩到破關。即使她的這條路將會帶來加速世界的結束，你想和她並肩作戰的意志也不會動搖嗎？」

「對。畢竟那是她『該保護的東西』……而且我認為即使有誰升上了10級，這個世界也不會就這麼突然消失。等級這種東西，不就是為了贏得某些東西才升上去的嗎？如果這個遊戲有簡單到只要增加數字就可以破關，我們才不會玩得這樣拚命哭泣、拚命歡笑。就算升上了10級，見到了所謂開發者，我想頂多也只會領到一些新的任務而已。」

說來對黑之王有點過意不去，但聽仁子這麼一斷定，就愈想愈覺得多半真是這樣。但相對的，美早卻也沒有足夠的自負，敢斷定BRAIN　BURST的10級只是個數字。畢竟1（有加級，是不管灌注多少超頻點數都升下上去的。只有走過讓五名9級玩家點數全失的這種血腥的道路，才能去到那個境界——

仁子將黑之王Black　Lotus要走的霸道，形容為「該保護的東西」。相信仁子一定也在黑之王身上，感受到了某種與她自己相近的事物。她們兩人雖然一個用劍，一個用槍，但兩個對戰虛擬角色，都懷抱著傷害、驅逐所有接近自己之事物的力量而誕生。而她們兩人都正視、接受象徵自己「精神創傷」的力量，試圖將傷痛化為堅強——將黑暗化為光明。

「……哪怕……將來有一天，非得和Lotus打不可的那個時候會來臨，也不例外……」

仁子以一雙蘊含著堅定光輝的鏡頭眼，依序看著不發一語的三獸士，以鎮定的嗓音說道。

「即使結果會導致我們當中有一個人點數全失，我也會接受這個命運。因為明知有一戰定生死這個規則，卻還選擇升上9級的就是我自己……我之所以追求力量，是為了保護我周遭的世界。所以，我想把這個決定貫徹到最後關頭。為了保護我要保護的東西，現在我非得和Lotus通力合作，和白之團戰鬥不可。」

仁子閉上嘴後，Thistle與Cassis仍然好一會兒什麼話都不說。

Thistle將一身不知不覺間已經再生完畢的毛皮豎起一次，然後變回柔軟，任由毛皮受冷風吹拂。

這個薊色的對戰虛擬角色，仰望著翻騰的烏雲，花了很長的時間慢慢點頭。

「……既然Rain有這樣的覺悟，我們也只能奉陪到底了吧……Blaze這些老團員，就交給我去說服。相信只要好好談，他們也一定會懂的。」

她的聲調中有著兩種等量的感情。既有著對不確定的未來所懷抱的不安、對強大敵人的畏懼，同時也有著只有選擇抗戰的人才會擁有的決心。

Cassis　Moosea帶響一身厚實的裝甲，點頭答應：

「我一直覺得這一天遲早會來……要離開領土戰鬥的這一天。雖然形式出乎我意料之外，但我也會聽從軍團長的意思。」

聽他們兩人表明心意，仁子用力點了點頭回應，將握緊拳頭的右手往前伸出：

「謝謝你們，波奇、卡西……還有Pard。無論以後狀況怎麼改變，我向你們保證，我絕對不會逃避。哪怕對手是白之王，是災禍之鎧MarkⅡ，我絕對不會做出嚇得後退這種事。我會全力奮戰到最後關頭。」

Thistle與Cassis默默走上前，把自己的拳頭往仁子小小的拳頭碰去。

——仁子，你變堅強了。

美早在心中這麼喃喃自語，自己也靠近他們三人，把拳頭碰了上去。

轟雷空間的烏雲開出了一道小小的縫隙，從中灑下的光芒，將四個拳頭照得就像火焰般閃閃發光。
