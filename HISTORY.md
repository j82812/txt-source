# HISTORY

## 2019-11-18

### Epub

- [王国へ続く道](cm/%E7%8E%8B%E5%9B%BD%E3%81%B8%E7%B6%9A%E3%81%8F%E9%81%93) - cm
  <br/>( v: 1 , c: 7, add: 1 )

### Segment

- [王国へ続く道](cm/%E7%8E%8B%E5%9B%BD%E3%81%B8%E7%B6%9A%E3%81%8F%E9%81%93) - cm
  <br/>( s: 5 )

## 2019-11-17

### Epub

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( v: 4 , c: 101, add: 3 )
- [葉隠桜は嘆かない](ts/%E8%91%89%E9%9A%A0%E6%A1%9C%E3%81%AF%E5%98%86%E3%81%8B%E3%81%AA%E3%81%84) - ts
  <br/>( v: 3 , c: 85, add: 17 )

### Segment

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( s: 3 )
- [葉隠桜は嘆かない](ts/%E8%91%89%E9%9A%A0%E6%A1%9C%E3%81%AF%E5%98%86%E3%81%8B%E3%81%AA%E3%81%84) - ts
  <br/>( s: 17 )

## 2019-11-15

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 93, add: 0 )

## 2019-11-14

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 93, add: 0 )

## 2019-11-13

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 93, add: 0 )

## 2019-11-12

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 93, add: 1 )

## 2019-11-10

### Epub

- [大叔的重生冒險日記](user2/%E5%A4%A7%E5%8F%94%E7%9A%84%E9%87%8D%E7%94%9F%E5%86%92%E9%9A%AA%E6%97%A5%E8%A8%98) - user2
  <br/>( v: 6 , c: 49, add: 0 )

## 2019-11-04

### Epub

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( v: 3 , c: 98, add: 9 )

### Segment

- [嘆きの亡霊は引退したい](syosetu/%E5%98%86%E3%81%8D%E3%81%AE%E4%BA%A1%E9%9C%8A%E3%81%AF%E5%BC%95%E9%80%80%E3%81%97%E3%81%9F%E3%81%84) - syosetu
  <br/>( s: 9 )

## 2019-10-24

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 92, add: 0 )

## 2019-10-23

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 92, add: 1 )

## 2019-10-19

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 91, add: 0 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2019-10-18

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 91, add: 0 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2019-10-17

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 91, add: 1 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )



